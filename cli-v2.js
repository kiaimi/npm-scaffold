#!/usr/bin/env node

/**
 * v1 生成文件夹，并且将配置字段替换进去，将index文件名更改成默认文件名(文件层级仅为一层)
 * v2 改用异步函数async/await 避免回调地狱写法 读取嵌套文件夹并生成
 */

// 使用 NodeJS 完成一个自定义的小型脚手架工具 
const inquirer = require('inquirer');
const ejs = require('ejs')
const path = require('path')
const fs = require('fs')

const cwd = process.cwd()

// inquirer.prompt()返回的是Promise对象
inquirer.prompt([{
  type: 'input',
  name: 'fileName',
  default: 'index',
  message: '文件名称:'
}, {
  type: 'input',
  name: 'folderName',
  default: 'base',
  message: '文件夹名称:'
}]).then(async (res) => {
  const temps = path.join(__dirname, 'templates-v2')
  const outputFolder = path.join(cwd, res.folderName)

  if (!fs.existsSync(outputFolder)) {
    // 补充目标保存文件夹
    await fs.mkdirSync(outputFolder, {
      recursive: true
    })
  }

  const files = await fs.readdirSync(temps, {
    recursive: true
  })
  files.forEach(async (x) => {
    const targetPath = path.join(temps, x)
    const stat = await fs.statSync(targetPath)
    // console.log(x)
    const outputName = x.replace('index', res.fileName)

    if (!stat.isFile()) {
      try {
        await fs.mkdirSync(path.join(outputFolder, outputName))
      } catch (err) {
        // console.log(`已有相同文件夹${outputFolder}-${outputName},跳过该文件夹生成`)
      } finally {}
    } else {
      const data = await ejs.renderFile(targetPath, res)
      fs.writeFileSync(path.join(outputFolder, outputName), data)
    }
  })
  console.log('模板已生成')
})