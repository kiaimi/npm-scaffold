#!/usr/bin/env node

/**
 * v1 生成文件夹，并且将配置字段替换进去，将index文件名更改成默认文件名(文件层级仅为一层)
 */

// 使用 NodeJS 完成一个自定义的小型脚手架工具 
const inquirer = require('inquirer');
const ejs = require('ejs')
const path = require('path')
const fs = require('fs')

const cwd = process.cwd()

// inquirer.prompt()返回的是Promise对象
inquirer.prompt([{
  type: 'input',
  name: 'fileName',
  default: 'index',
  message: '文件名称:'
}, {
  type: 'input',
  name: 'folderName',
  default: 'base',
  message: '文件夹名称:'
}]).then((res) => {
  const temps = path.join(__dirname, 'templates-v1')

  // 读取模板目录，获取所有文件
  fs.readdir(temps, (err, files) => {
    // console.log(files)
    if (err) throw err
    // 遍历所有文件，把用户输入的信息，通过ejs渲染到文件内
    files.forEach((item) => {
      /*
        1.更改文件名 index => 默认文件名
       */
      // console.log(item) // index.html
      const outputName = item.replace('index', res.fileName)
      const outputFolder = path.join(cwd, res.folderName)
      ejs.renderFile(path.join(temps, item), res, (err, successData) => {
        if (err) throw err
        // 把通过ejs渲染后的文件内容，写入到目标目录下
        fs.writeFile(path.join(outputFolder, outputName), (err, fd) => {
          if (err && err.code === 'ENOENT') {
            // 文件夹不存在
            fs.mkdir(outputFolder, (err, fd) => {

              fs.writeFile(path.join(outputFolder, outputName), (err) => {
                console.log(err)
              })
            })
          }
        })
      })
    })
  })


})

const newFolder = (path) => {
  fs.mkdir(path)
}